﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Data;

namespace LatihanDB
{
    class MahasiswaDAO
    {
        private MySqlCommand perintah = null;
        string konfigurasi = "Server=localhost;Port=3306;UID=root;PWD=;Database=dbpv";
        MySqlConnection koneksi = new MySqlConnection();

        public MahasiswaDAO()
        {
            koneksi.ConnectionString = konfigurasi;
        }

        public DataSet getData()
        {
            DataSet ds = new DataSet();
            try
            {
                koneksi.Open();
                perintah = new MySqlCommand();
                perintah.Connection = koneksi;
                perintah.CommandType = CommandType.Text;
                perintah.CommandText = "SELECT nim,nama,fakultas FROM tb_mhs";
                MySqlDataAdapter mdap = new MySqlDataAdapter(perintah);
                mdap.Fill(ds, "tb_mhs");
                koneksi.Close();
            }catch(MySqlException){
            }            
            return ds;
        }


        public bool insertData(Mahasiswa m)
        {
            Boolean stat = false;
            try
            {
                koneksi.Open();
                perintah = new MySqlCommand();
                perintah.Connection = koneksi;
                perintah.CommandType = CommandType.Text;
                perintah.CommandText = "INSERT INTO tb_mhs VALUES ('"+m.Nim+"','"+m.Nama+"','"+m.Fakultas+"')";
                perintah.ExecuteNonQuery();
                stat = true;
                koneksi.Close();
            }
            catch (MySqlException) { }

            return stat;
        }

        public bool deleteData(string nim)
        {
            Boolean stat = false;
            try
            {
                koneksi.Open();
                perintah = new MySqlCommand();
                perintah.Connection = koneksi;
                perintah.CommandType = CommandType.Text;
                perintah.CommandText = "DELETE FROM tb_mhs WHERE nim='"+nim+"'";
                perintah.ExecuteNonQuery();
                stat = true;
                koneksi.Close();
            }
            catch (MySqlException) { }
            return stat;
        }

        public bool updateData(Mahasiswa m, string nim){
            Boolean stat = false;
            try
            {
                koneksi.Open();
                perintah = new MySqlCommand();
                perintah.Connection = koneksi;
                perintah.CommandType = CommandType.Text;
                perintah.CommandText = "UPDATE tb_mhs SET nim='"+m.Nim+"', nama='"+m.Nama+"',fakultas='"+m.Fakultas+"' WHERE nim='"+nim+"'";
                perintah.ExecuteNonQuery();
                stat = true;
                koneksi.Close();
            }
            catch (MySqlException) { }
            return stat;
        }

    }
}
