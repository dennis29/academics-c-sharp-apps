﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LatihanDB
{
    class Mahasiswa
    {
        private string nim, nama, fakultas;

        public string Fakultas
        {
            get { return fakultas; }
            set { fakultas = value; }
        }

        public string Nama
        {
            get { return nama; }
            set { nama = value; }
        }

        public string Nim
        {
            get { return nim; }
            set { nim = value; }
        }


    }
}
