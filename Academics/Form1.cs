﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LatihanDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        MahasiswaDAO md = new MahasiswaDAO();

        void lihatSemuaData()
        {
            DataSet data = md.getData();
            dataGridView1.DataSource = data;
            dataGridView1.DataMember = "tb_mhs";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lihatSemuaData();
        }

        private void insertBtn_Click(object sender, EventArgs e)
        {
            Mahasiswa m = new Mahasiswa();
            m.Nim = nimTxb.Text;
            m.Nama = namaTxb.Text;
            m.Fakultas = fakultasTxb.Text;
            md.insertData(m);
            lihatSemuaData();
        }

        string nim;
        private void updateBtn_Click(object sender, EventArgs e)
        {
            Mahasiswa m = new Mahasiswa();
            m.Nim = nimTxb.Text;
            m.Nama = namaTxb.Text;
            m.Fakultas = fakultasTxb.Text;
            md.updateData(m,nim);
            lihatSemuaData();
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            nimTxb.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            namaTxb.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            fakultasTxb.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();

            nim = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
        }

        private void deleteBtn_Click(object sender, EventArgs e)
        {
            md.deleteData(nim);
            lihatSemuaData();
        }
    }
}
